// Code based on http://stackoverflow.com/questions/20730901/create-more-than-one-window-of-a-single-sketch-in-processing
import oscP5.*;
import netP5.*;
import javax.swing.*; 

/*

This example does not use any clever configuration.

All the values are hard-coded.  You'll have to find them in the
code to change them.

It exists as a simple example to use
with a corresponding TWO demo.

*/


ExtraApplet extraApplet;
OscP5       oscP5;
NetAddress  oscServer;

String serverAddressPattern = "127.0.0.1";

int listeningPort = 7000;
int sendingPort = 1000;

int currentColor        = 9;
int instance            = 0;
float currentSaturation = 100;


void setup() {
  size(640, 480);
  PFrameOsc frame2 = new PFrameOsc(width, height);
  frame2.configAndShow(1, 7001, 1001, serverAddressPattern);
  frame.setTitle("Screen 0");
  frame2.setTitle("Screen 1");
  oscP5 = new OscP5(this, listeningPort);
  oscServer = new NetAddress(serverAddressPattern, sendingPort);
}


void draw() {
  colorMode(HSB, 100);
  sendMessage("/demo/state", instance, currentColor, currentSaturation );
  background(currentColor, currentSaturation, 100);
}


void setColor(int i) {
  println("setColor = " + i);
  currentColor = 10 * constrain(i, 1, 10);
}

void setSaturation(float f) {
  println("setSaturation = " + f);
  currentSaturation = 100 * constrain(f, 0.0, 1.0);
}

void sendMessage(String addressPattern, int instance, int currentColor, float opacity) {
  OscMessage msg = new OscMessage(addressPattern);
  msg.add(instance);
  msg.add(currentColor);
  msg.add(opacity);
  oscP5.send(msg, oscServer); 
}

void oscEvent(OscMessage msg) {

  print("Received an osc message. ");
  print("Addrpattern: "+msg.addrPattern());
  println("; typetag: "+msg.typetag());

  if (msg.checkAddrPattern("/demo/color") == true  ) {
    if(msg.checkTypetag("i")) {
      setColor(msg.get(0).intValue() );
      return;
    }
  }

  if (msg.checkAddrPattern("/demo/saturation") == true  ) {
    if(msg.checkTypetag("f")) {
      setSaturation(msg.get(0).floatValue() );
      return;
    }
  }
  println("Do not know what to do with " + msg.addrPattern() + " " + msg.typetag() );
}

/*****************************************************************/

public class PFrameOsc extends JFrame {

  OscP5       oscP5;
  NetAddress  oscServer;

  String serverAddressPattern = "127.0.0.1";

  int listeningPort = 7001;
  int sendingPort = 1001;
  int instance;
  private ExtraApplet extraApplet;

  public PFrameOsc(int width, int height) {
    setBounds(100, 100, width, height);
    this.extraApplet = new ExtraApplet();
    println("Create new extraApplet ");
    add(extraApplet);
  }

  public void configAndShow(int instance, int listeningPort, int sendingPort, String serverAddressPattern) {
    extraApplet.config(instance, listeningPort, sendingPort, serverAddressPattern);
    extraApplet.init();
    show();
  }
}

/*****************************************************************/
public class ExtraApplet extends PApplet {

  OscP5       oscP5;
  NetAddress  oscServer;

  String serverAddressPattern = "127.0.0.1";

  int listeningPort;
  int sendingPort;

  int currentColor        = 2;
  int instance            = 0;
  float currentSaturation = 100;

  public void config(int instance, int listeningPort, int sendingPort, String serverAddressPattern ) {
    this.listeningPort = listeningPort;
    this.sendingPort = sendingPort;
    this.instance = instance;
    this.serverAddressPattern  = serverAddressPattern ;
    this.oscP5 = new OscP5(this, this.listeningPort);
    this.oscServer = new NetAddress(this.serverAddressPattern, this.sendingPort);
    println("Instance " +this.instance+ " listening on port " +this.listeningPort+ " and sending on port " + this.sendingPort);

  }

  public void setup() {
  }

  public void draw() {
    this.colorMode(HSB, 100);
    this.sendMessage("/demo/state", this.instance, this.currentColor, this.currentSaturation );
    this.background(this.currentColor, this.currentSaturation, 100);
  }

  void setColor(int i) {
    println("setColor = " + i);
    this.currentColor = 10 * constrain(i, 1, 10);
  }

  void setSaturation(float f) {
    println("setSaturation = " + f);
    this.currentSaturation = 100 * constrain(f, 0.0, 1.0);
  }

  void sendMessage(String addressPattern, int instance, int currentColor, float opacity) {
    OscMessage msg = new OscMessage(addressPattern);
    msg.add(instance);
    msg.add(currentColor);
    msg.add(opacity);
    this.oscP5.send(msg, this.oscServer); 
  }

  void oscEvent(OscMessage msg) {

    print("Received an osc message. ");
    print("Addrpattern: "+msg.addrPattern());
    println("; typetag: "+msg.typetag());

    if (msg.checkAddrPattern("/demo/color") == true  ) {
      if(msg.checkTypetag("i")) {
        setColor(msg.get(0).intValue() );
        return;
      }
    }

    if (msg.checkAddrPattern("/demo/saturation") == true  ) {
      if(msg.checkTypetag("f")) {
        this.setSaturation(msg.get(0).floatValue() );
        return;
      }
    }
    println("Do not know what to do with " + msg.addrPattern() + " " + msg.typetag() );
  }
}
