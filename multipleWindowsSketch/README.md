# Multiple Windows Sketch 

This is a demo sketch to used as part of TWO demo.

It is meant to emulate running two separate OSC programs.

Each of them behave the same:

- Show a windows with a solid color
- Handle OSC messages to change the color and color saturation
- Send an OSC status message on each call to `draw`, with the current instance number, color, and saturation values


`instance` is just an identifier; in the sketch these are set to 0 and 1.

Instance 0 listen on port 7000 and sends on port 1000

Instance 1 listen on port 7001 and sends on port 1001

The accepted OSC messages are:

    /demo/color i
    /demo/saturation f

The status message is: 

    /demo/state iif
 
(Instance, color, and saturation)

All the OSC values are hard-coded.  The sketch assumes that TWO is available on the same machine (127.0.0.1).

Accepted color values are from 0 to 10.  Values out of that range are forced to either 0 or 10.

Accepted saturation values are from 0.0 to 1.0.  Values out of that range are forced to either 0.0 or 1.0.

Once you run the sketch you should see two separate windows.  The window colors and saturation should change depending the messages sent to the different ports.

** Important **

Because of the code used to create two distinct windows, this sketch only works with Processing version 2.

