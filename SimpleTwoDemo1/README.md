# Simple  OSC Sketch 

This is a demo sketch to used as part of TWO demo.

The sketchy should

- Show a windows with a solid color
- Handle OSC messages to change the color and color saturation
- Send an OSC status message on each call to `draw`, with the current instance number, color, and saturation values

`instance` is just an identifier; in the sketch these are set to 0 and 1.

The accepted OSC messages are:

    /demo/color i
    /demo/saturation f

The status message is: 

    /demo/state iif
 
(Instance, color, and saturation)

Accepted color values are from 0 to 10.  Values out of that range are forced to either 0 or 10.

Accepted saturation values are from 0.0 to 1.0.  Values out of that range are forced to either 0.0 or 1.0.

To use this sketch with the TWO demo you need to run two separate instances.

The sketch will read in a configuration file (`data/config.jsi`) that uses a simplified form of JSON.

One instance should listen on port 7000 and send on port 1000, and have an `instance` value of 0.

The other instance should listen on port 7001 and send on port 1001, and have an `instance` value of 1.

The demo was created on the assumption that TWO is running in the same machine as the sketches (127.0.0.1).

The `config.jsi` file should look like this:

    oscListeningPort: 7000
    oscServerPort: 1000
    oscServerIP: 127.0.0.1
    instance:1
    serverAddressPattern:"/TWO/demo/status"

Adjust these value for each instance you run.
