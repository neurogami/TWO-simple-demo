import oscP5.*;
import netP5.*;

Configgy    config;
OscP5       oscP5;
NetAddress  oscServer;
String      serverAddressPattern;
int listeningPort;
int sendingPort;

int currentColor        = 30;
int instance            = 0;
float currentSaturation = 100;

void setup() {
  size(400, 400);
  config = new Configgy("config.jsi");
  serverAddressPattern = config.getString("serverAddressPattern");
  instance = config.getInt("instance");
  listeningPort =  config.getInt("oscListeningPort");
  sendingPort =  config.getInt("oscServerPort");
  oscP5 = new OscP5(this, listeningPort);
  oscServer = new NetAddress(config.getString("oscServerIP"), sendingPort);
  println("Instance " +instance+ " listening on port " +listeningPort+ " and sending on port " + sendingPort);
}

void draw() {
  colorMode(HSB, 100);
  sendMessage("/demo/state", instance, currentColor, currentSaturation );
  background(currentColor, currentSaturation, 100);
}

void setColor(int i) {
  println("setColor = " + i);
  currentColor = 10 * constrain(i, 1, 10);
}

void setSaturation(float f) {
  println("setSaturation = " + f);
  currentSaturation = 100 * constrain(f, 0.0, 1.0);
}

void sendMessage(String addressPattern, int instance, int currentColor, float opacity) {
  OscMessage msg = new OscMessage(addressPattern);
  msg.add(instance);
  msg.add(currentColor);
  msg.add(opacity);
  oscP5.send(msg, oscServer); 
}

void oscEvent(OscMessage msg) {

  print("Received an osc message. ");
  print("Addrpattern: "+msg.addrPattern());
  println("; typetag: "+msg.typetag());

  if (msg.checkAddrPattern("/demo/color") == true  ) {
    if(msg.checkTypetag("i")) {
      setColor(msg.get(0).intValue() );
      return;
    }
  }

  if (msg.checkAddrPattern("/demo/saturation") == true  ) {
    if(msg.checkTypetag("f")) {
      setSaturation(msg.get(0).floatValue() );
      return;
    }
  }
  println("Do not know what to do with " + msg.addrPattern() + " " + msg.typetag() );
}
