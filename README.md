# Simple demo for TWO

An attempt to understand TWO ((The Wizard of OSC)[http://thewizardofosc.com/]) by writing a simple demo.

The demo uses a Processing sketch that responds to two simple OSC messages.


__There are two different sketches__

The initial version of this demo required you to run two separate instances of the same sketch.

The current version adds a new sketch that has two windows, and handles OSC messages on two different ports.

It essentially combines the first sketch into a self-contained two-program instance.  

There is no configuration for this one, but it only works with Processing version 2.

Each sketch has a README file that explains how to use them.  The TWO demo whould work essentially the same with either one.



# Running the Processing sketch

You'll need to install [Processing](https://processing.org/).  The multi-window sketch only works with Processing 2.

You will need to have installed two contributed Processing libraries: OSCP5, and NetP5.



# Setting up TWO

Start TWO.  Everything should be (more or less) a blank slate.  

Switch to the `Namespace` tab.  We will create the namespace for the OSC messages handled by the Processing sketch.

Click `Create Namespace`. When prompted, select `OSC Namespace` to create. The default ID is `Root01`.  

You can leave this as-is, but you would do well to give things descriptive names.  Over on the right side you can change the ID to "Demo Sketch" (or whatever you like).

Set the `Address Part` to `demo`.  This corresponds to the first segment of the OSC messages handled by the Processing sketch.

Change `Direction` to "Destination", since the sketch only accepts this OSC, but does not send it.

Don't worry abut `Continuity` for now; it likely has no role for this demo.  (See the TWO manual for an explanation of continuity.)

Go back over to the root node, select it, then click on `Create Node`.

__WARNING! Some actions have no undo. This is a known issue and will be fixed.__

The new node created under the root node will start showing the address pattern.  

This new node defaults to `AP01`. Change this to `color`.

`/demo/color` is the complete address pattern so we now need to define the type tag.  This is going to be a single integer.

With this new node highlighted, click on `Create Type Tag String`


Then click on `Create Type Tag`. Select this new type tag and change it (over on the right) so that the Type Tag is `Integer`.

`Use Range` is selected by default; change the range to be 1 to 10. 

__Note: The range fields are F D T, which mean "From", "Default", and "To"__

Now we want to create an entry for `/demo/saturation`

The process is the same (start from the root node, add a new node to that, change the address to `saturation`). The big difference is that the type tag will be a float, and we want a range of 0.0  to 1.0.

__At various places in TWO there are fields for an ID.  They do not need to be unique across the application, but they (currently) cannot be left blank and they cannot contain spaces.  IDs are used internally by TWO.  __

## Scene addresses 

Next we will set up address for the sketches.  We assume that the sketch code will be listening on ports 7000 and 7001, and sending on ports 1000 and 1001.

Over on the left create a new `Address`.  Make it an `OSC Root` address.  Over on the right, edit the settings.  Give it a meaningful ID.


Select a namespace to associate with this address. There's only one, so the choice is easy.

Leave `Location` as it is for now; we will go back and set this up in a moment.

Remove the entry for `OSC AP`.  Whatever value is there gets prepended to any OSC sent using this address.  We have already defined the complete address patterns our Processing sketch will recognize; adding anything to it would give us OSC messages not recognized by the sketch.

> Note: Any program that accepts OSC messages has some choices about how to handle what it gets.  Some programs, such as [Renoise](https://www.renoise.com/) or [Reaper](http://reaper.fm/) will only process the exact address patterns specified in their API.  

> Other programs, such as the [Mother](http://www.onar3d.com/mother) library for Processing, can look for matching sub-patterns in the OSC they receive.  Renoise, for example, will start playing when sent `/renoise/transport/start`. It will do nothing if sent `/foobar/renoise/transport/start`; the address pattern needs to be an exact match.  Keep this in mind when defining your namespaces and addresses.


## Network location 

Now create an `OSC Network Location` for our new address.

Set the ID to `demo_location_0`  Make sure `Root OSC` is empty.   Like `OSC AP` described above, any value here gets prepended to the address pattern.

Change `Outgoing Port` to 7000.  Change `Incoming Port` to 1000. These should be the values defined for the first instance of the demo Processing sketch.

Create another `Address` entry for the second sketch instance.  Again, make sue that `OSC AP` is empty. 

Then create a new `OSC Network Location`. Change `Outgoing Port` to 7001.  Change `Incoming Port` to 1001. These should be the values defined for the second demo Processing sketch.

You should now have two `Adddresses` entries, and two `OSC Network Locations` defined. 

> __Why not just create a different OSC Network location?__ 

> What seems to happen is that later, when you associate a Global Editor with  a `Location`, all such locations behave the same if they are linked to the same `OSC Network Location`.  Multiple editors end up sending everything to the same OSC address and port.  

## Live and Controllers

Now move to the `Live` tab.

Over on the left, just below where it says `Controllers`, click on `Create`.  Select `Global Editor`, then `Create`.

TWO will create a new `Editor`, automatically populated with the two `Address` entries created on the `Scene` tab, and with the `Namespace` previously defined.

You should now be able to use TWO to send messages to either of the Processing OSC listeners sketches .

You can click on the editor pane for the `/demo/color` AP and set the value to you want send.

The pane for the `/demo/saturation` AP acts like a slider.


__Settings, and Build Namespaces__

> By default, under `Settings`, `Build Namespaces` is checked.
> Initially, when the Processing sketch was run,  this was cause TWO to automatically add a new entry to the Namespace: /demo/demo/state.
> It also added an item with that same address pattern to the Global Editor.
> To prevent this you need to turn off Build Namespaces.


__Note: The saturation slider does not stop when it hits '1.0'.__
> Ilias Bergström explaind, "The clip setting ensures that the value going out, does not go beyond the ranges you have set. But the values in the controllers, should still be allowed to go beyond those ranges, due to how they interrelate with the matrix."


